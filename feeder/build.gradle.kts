
plugins {
    idea
    java
    application
}

application {
    mainClassName = "com.argotools.memetrics.fitbit.FitbitFeeder"
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

dependencyLocking {
    lockMode.set(LockMode.STRICT)
    lockAllConfigurations()
}

dependencies {
    implementation(platform("org.apache.logging.log4j:log4j-bom:2.+"))
    implementation("org.apache.logging.log4j:log4j-api")
    implementation("org.apache.logging.log4j:log4j-core")
    runtimeOnly("org.apache.logging.log4j:log4j-slf4j-impl")

    implementation(platform("com.fasterxml.jackson:jackson-bom:2.+"))
    implementation("com.fasterxml.jackson.core:jackson-databind")
    runtimeOnly("com.fasterxml.jackson.datatype:jackson-datatype-jdk8")
    runtimeOnly("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")

    implementation("com.influxdb:influxdb-client-java:1.+")

    testImplementation(platform("org.junit:junit-bom:5.+"))
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
    testImplementation("org.assertj:assertj-core:3.+")
}

repositories {
    mavenCentral {
        mavenContent {
            releasesOnly()
        }
    }
}
