package com.argotools.memetrics.fitbit.record;

import com.argotools.memetrics.fitbit.Record;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public abstract class RecordBase implements Record {

    @JsonFormat(pattern = "MM/dd/yy HH:mm:ss", timezone = "UTC")
    @JsonProperty(value = "dateTime", required = true)
    protected Instant dateTime;

}
