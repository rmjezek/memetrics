package com.argotools.memetrics.fitbit.record;

public final class Calories extends ScalarRecord {

    @Override
    protected String measurement() {
        return "calories";
    }
}
