package com.argotools.memetrics.fitbit.record;

public final class SedentaryMinutes extends ScalarRecord {

    @Override
    protected String measurement() {
        return "sedentary_minutes";
    }
}
