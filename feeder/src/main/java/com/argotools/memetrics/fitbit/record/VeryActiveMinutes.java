package com.argotools.memetrics.fitbit.record;

public final class VeryActiveMinutes extends ScalarRecord {

    @Override
    protected String measurement() {
        return "very_active_minutes";
    }
}
