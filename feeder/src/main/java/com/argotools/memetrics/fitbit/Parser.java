package com.argotools.memetrics.fitbit;

import java.io.InputStream;
import java.util.function.Consumer;

public interface Parser {

    boolean matches(String fileName);
    void parse(InputStream inputStream, Consumer<Record> recordConsumer) throws Exception;

}
