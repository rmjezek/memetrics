package com.argotools.memetrics.fitbit.record;

public final class Distance extends ScalarRecord {

    @Override
    protected String measurement() {
        return "distance";
    }
}
