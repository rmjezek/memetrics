package com.argotools.memetrics.fitbit.record;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import java.time.LocalDate;
import java.util.stream.Stream;

public final class RestingHeartRate extends RecordBase {

    /*
{
  "dateTime" : "08/23/18 00:00:00",
  "value" : {
    "date" : "08/23/18",
    "value" : 82.36728954315186,
    "error" : 16.177358627319336
  }
}
     */

    @JsonProperty("value")
    private Value value;

    @Override
    public Stream<Point> toPoints() {
        return Stream.of(Point.measurement("resting_heart_rate")
                .time(dateTime.toEpochMilli(), WritePrecision.MS)
                .addField("value", value.value)
                .addField("error", value.error)
        );
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    private static final class Value {

        @JsonFormat(pattern = "MM/dd/yy")
        LocalDate date;
        Double value;
        Double error;

    }
}
