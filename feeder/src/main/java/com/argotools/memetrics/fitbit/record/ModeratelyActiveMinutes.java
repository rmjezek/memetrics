package com.argotools.memetrics.fitbit.record;

public final class ModeratelyActiveMinutes extends ScalarRecord {

    @Override
    protected String measurement() {
        return "moderately_active_minutes";
    }
}
