package com.argotools.memetrics.fitbit.record;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import java.util.stream.Stream;

public class HeartRate extends RecordBase {
   /*
    {
        "dateTime" : "08/21/18 22:00:28",
        "value" : {
            "bpm" : 73,
            "confidence" : 3
        }
    }
    */

    @JsonProperty("value")
    private Value value;

    @Override
    public Stream<Point> toPoints() {
        return Stream.of(Point.measurement("heart_rate")
                .time(dateTime.toEpochMilli(), WritePrecision.MS)
                .addField("bpm", value.bpm)
                .addField("confidence", value.confidence)
        );
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    private static final class Value {

        Double bpm;
        Double confidence;

    }
}
