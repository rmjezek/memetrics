package com.argotools.memetrics.fitbit.record;

import com.argotools.memetrics.fitbit.Record;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.stream.Stream;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public final class Weight implements Record {

    /*
{
  "logId" : 1545263999000,
  "weight" : 186.9,
  "bmi" : 26.76,
  "fat" : 26.799999237060547,
  "date" : "12/19/18",
  "time" : "23:59:59",
  "source" : "API"
}
     */

    private static final double LB_TO_KG = 0.453592;

    private Long logId;
    private Double weight;
    private Double bmi;
    private Double fat;
    @JsonFormat(pattern = "MM/dd/yy")
    private LocalDate date;
    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime time;
    private String source;

    @Override
    public Stream<Point> toPoints() {
        return Stream.of(Point.measurement("weight")
                .time(date.atTime(time).toInstant(ZoneOffset.UTC).toEpochMilli(), WritePrecision.MS)
                .addField("value", weight * LB_TO_KG)
                .addField("bmi", bmi)
                .addField("fat", fat)
        );
    }
}
