package com.argotools.memetrics.fitbit;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import java.io.InputStream;
import java.util.function.Consumer;

public class JsonArrayParser implements Parser {

    private static final ObjectMapper objectMapper = new ObjectMapper()
            .findAndRegisterModules()
            .disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);

    private final String prefix;
    private final ObjectReader objectReader;

    public JsonArrayParser(String prefix, Class<? extends Record> recordType) {
        this.prefix = prefix;
        this.objectReader = objectMapper.readerFor(recordType);
    }

    @Override
    public boolean matches(String fileName) {
        return fileName.startsWith(prefix) && fileName.endsWith(".json");
    }

    @Override
    public void parse(InputStream inputStream, Consumer<Record> recordConsumer) throws Exception {
        MappingIterator<Record> iterator = objectReader.readValues(inputStream);
        while (iterator.hasNextValue()) {
            recordConsumer.accept(iterator.nextValue());
        }
    }
}
