package com.argotools.memetrics.fitbit.record;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import java.util.stream.Stream;

public abstract class ScalarRecord extends RecordBase {

    @JsonProperty("value")
    private double value;

    @Override
    public final Stream<Point> toPoints() {
        return Stream.of(Point.measurement(measurement())
                .time(dateTime.toEpochMilli(), WritePrecision.MS)
                .addField("value", value)
        );
    }

    protected abstract String measurement();

}
