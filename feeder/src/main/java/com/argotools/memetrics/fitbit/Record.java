package com.argotools.memetrics.fitbit;

import com.influxdb.client.write.Point;

import java.util.stream.Stream;

public interface Record {

    Stream<Point> toPoints();

}
