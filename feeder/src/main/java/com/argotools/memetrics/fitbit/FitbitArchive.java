package com.argotools.memetrics.fitbit;

import com.argotools.memetrics.fitbit.record.Calories;
import com.argotools.memetrics.fitbit.record.DemographicVO2Max;
import com.argotools.memetrics.fitbit.record.Distance;
import com.argotools.memetrics.fitbit.record.HeartRate;
import com.argotools.memetrics.fitbit.record.LightlyActiveMinutes;
import com.argotools.memetrics.fitbit.record.ModeratelyActiveMinutes;
import com.argotools.memetrics.fitbit.record.RestingHeartRate;
import com.argotools.memetrics.fitbit.record.SedentaryMinutes;
import com.argotools.memetrics.fitbit.record.Steps;
import com.argotools.memetrics.fitbit.record.VeryActiveMinutes;
import com.argotools.memetrics.fitbit.record.Weight;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public final class FitbitArchive {

    private static final Logger logger = LogManager.getLogger();

    private static final List<com.argotools.memetrics.fitbit.Parser> PARSERS = Arrays.asList(
            new JsonArrayParser("calories", Calories.class),
            new JsonArrayParser("demographic_vo2_max", DemographicVO2Max.class),
            new JsonArrayParser("distance", Distance.class),
            new JsonArrayParser("heart_rate", HeartRate.class),
            new JsonArrayParser("lightly_active_minutes", LightlyActiveMinutes.class),
            new JsonArrayParser("moderately_active_minutes", ModeratelyActiveMinutes.class),
            new JsonArrayParser("resting_heart_rate", RestingHeartRate.class),
            new JsonArrayParser("sedentary_minutes",SedentaryMinutes .class),
            new JsonArrayParser("steps",Steps .class),
            new JsonArrayParser("very_active_minutes", VeryActiveMinutes.class),
            new JsonArrayParser("weight", Weight.class)
    );

    public static void parse(InputStream inputStream, Consumer<Record> consumer) throws Exception {
        try (ZipInputStream zipInputStream = new ZipInputStream(inputStream)) {
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                String name = entryToFileName(entry);
                for (com.argotools.memetrics.fitbit.Parser parser : PARSERS) {
                    if (parser.matches(name)) {
                        logger.info("Loading {} ...", name);
                        parser.parse(zipInputStream, consumer);
                        break;
                    }
                }
            }
        }
    }

    private static String entryToFileName(ZipEntry entry) {
        String path = entry.getName();
        int lastSlash = path.lastIndexOf('/');
        if (lastSlash >= 0) {
            return path.substring(lastSlash + 1);
        } else {
            return path;
        }
    }
}
