package com.argotools.memetrics.fitbit.record;

public final class LightlyActiveMinutes extends ScalarRecord {

    @Override
    protected String measurement() {
        return "lightly_active_minutes";
    }
}
