package com.argotools.memetrics.fitbit.record;

public final class Steps extends ScalarRecord {

    @Override
    protected String measurement() {
        return "steps";
    }
}
