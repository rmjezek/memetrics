package com.argotools.memetrics.fitbit.record;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;

import java.util.stream.Stream;

public final class DemographicVO2Max extends RecordBase {

    /*
    {
  "dateTime" : "08/20/18 00:00:00",
  "value" : {
    "demographicVO2Max" : 37.47643,
    "demographicVO2MaxError" : 3.0000000000000004,
    "filteredDemographicVO2Max" : 37.47643,
    "filteredDemographicVO2MaxError" : 3.0000000000000004
  }
}
     */

    @JsonProperty("value")
    private Value value;

    @Override
    public Stream<Point> toPoints() {
        return Stream.of(Point.measurement("demographic_vo2_max")
                .time(dateTime.toEpochMilli(), WritePrecision.MS)
                .addField("value", value.demographicVO2Max)
                .addField("value_error", value.demographicVO2MaxError)
                .addField("filtered", value.filteredDemographicVO2Max)
                .addField("filtered_error", value.filteredDemographicVO2MaxError)
        );
    }

    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
    private static final class Value {

        Double demographicVO2Max;
        Double demographicVO2MaxError;
        Double filteredDemographicVO2Max;
        Double filteredDemographicVO2MaxError;

    }
}
