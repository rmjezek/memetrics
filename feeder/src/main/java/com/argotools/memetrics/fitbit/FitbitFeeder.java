package com.argotools.memetrics.fitbit;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.InfluxDBClientOptions;
import com.influxdb.client.WriteApi;
import com.influxdb.client.write.Point;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public final class FitbitFeeder {

    private static final int MAX_BATCH_SIZE = 1000;

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            System.out.println("No file specified");
            return;
        }
        Path sourceFile = Paths.get(args[0]);

        InfluxDBClientOptions options = InfluxDBClientOptions.builder()
                .url("http://localhost:8086")
                .bucket("db0")
                .org("test_org")
                .build();
        InfluxDBClient client = InfluxDBClientFactory.create(options);
        List<Point> buffer = new ArrayList<>(MAX_BATCH_SIZE);

        WriteApi writeApi = client.getWriteApi();
        try (InputStream inputStream = Files.newInputStream(sourceFile)) {
            FitbitArchive.parse(inputStream, r -> r.toPoints()
                    .forEach(point -> {
                        buffer.add(point);
                        if (buffer.size() == MAX_BATCH_SIZE) {
                            //System.out.println("Flushing");
                            writeApi.writePoints(buffer);
                            writeApi.flush();
                            buffer.clear();
                        }
                    }));
            writeApi.writePoints(buffer);
            writeApi.flush();
        }
    }
}
