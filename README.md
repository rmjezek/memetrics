# MeMetrics

## Prerequisites
 1. Fitbit account
 2. Docker Compose
 3. Java 8+

## Export Fitbit data
 1. Go to [Fitbit Dashboard](https://www.fitbit.com/). 
 2. Click cog icon > Settings.\
![](doc/settings.png)
 3. In settings click Data Export and then Request Data button.\
![](doc/data-export.png)
 4. You will receive an email with confirmation link.\
![](doc/confirm-mail.png)
 5. It will take some time to prepare the archive so make a cup of cocoa.
 6. Once the archive is ready, download it to your local drive.

## Start environment
The data will be stored in InfluxDB and visualized by Grafana, both running in Docker containers.
In the root directory run:
```bash
docker-compose up -d
```
Go to [localhost:3000](http://localhost:3000/) to verify Grafana is running and accessible.
The default username is `mr_grafana` and password is `pass-o-fana`.

## Ingest data
Run the Shell script with the path to your Fitbit archive file:
```bash
./ingest <path-to-MyFitbitData.zip>
```
This step may take several minutes to ingest all the data so be patient.

## Get insight
Go to [localhost:3000](http://localhost:3000/) and enjoy the immersive insight
into your health conditions.

## Go exercise
Stop the environment:
```bash
docker-compose down
```
And get moving!
